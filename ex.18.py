# this one is like your script with argv
def print_two(*args):
    arg1, arg2 = args
    print(f"arg1: {arg1} arg2: {arg2}")
# ok that *args is actually pointless, we can just do ths
def print_two_again(arg1, arg2):
    print(f"arg1, {arg1}, arg2, {arg2}")
    #ths just takes one argument
def print_one(arg1):
    print(f"arg1: {arg1}")
# this one takes no argument
def print_none():
    print("i got nothin'.")
print_two("Zed","shaw")
print_two_again("Zed","shaw")
print_one("First!")
print_none()