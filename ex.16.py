from sys import argv

script, filename = argv
print(f"we're going to erase {filename}.")
print("if you don't want that, hit ctrl-c (^c).")
print("if you do want that, hit return.")

input("?")

print("opening the file...")
target = open(filename, 'w')
print("truncating the file. goodbbye")
target.truncate()
print("now i'm going to ask you for three lines.")
line1 = input("line l:")
line2 = input("line 2:")
line3 = input("line 3:")
print("i'm going to write these to the file.")
target.write(line1)
target.write("\n")
target.write(line2)
target.write("\n")
target.write(line3)
target.write("\n")
print("and finaly, we close it.")
target.close()