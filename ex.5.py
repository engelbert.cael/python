my_name = 'Cael Engelbert'
my_age = 9
sixteen = 16
years_until_sixteen = sixteen - my_age

print(f'my name is {my_name} and I am {my_age}')
print(f'I will be 16 in {years_until_sixteen} years')
