def cheese_and_crakers(cheese_count, boxes_of_crakers):
    print(f"you have {cheese_count} cheese!")
    print(f"you have {boxes_of_crakers} boxes of crakers!")
    print("man that's enough for a party!")
    print("get a blanket.\n")

print("we ca just give the function numbers directly:")
cheese_and_crakers(20, 30)
   
print("OR, we can use variables from our sript:")

amount_of_cheese = 10
amount_of_crackers = 50

cheese_and_crakers(amount_of_cheese,amount_of_crackers)

print("we can even do math inside too")
cheese_and_crakers(10 + 20, 5 + 6)

print("and we can combine the two, variables and math")

cheese_and_crakers(amount_of_crackers + 100, amount_of_crackers + 1000)