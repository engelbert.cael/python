from sys import argv

script, filename = argv
print(f"we're going to erase {filename}.")
print("if you don't want that, hit ctrl-c (^c).")
print("if you do want that, hit return.")

input("?")

print("opening the file...")
target = open(filename, 'w')


family = {
    'simon': '38',
    'amy': '39',
    'rylan': '12',
    'asher': '11',
    'cael': '9',
    'eli': '5'
}

for name, age in list(family.items()):
   target.write(f"{name} is {age} years old\n")
   print(f"{name} is {age} years old")